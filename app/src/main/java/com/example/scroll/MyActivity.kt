package com.example.scroll

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_my.*

class MyActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my)
        init()
    }

    private fun init(){

        Glide.with(this).load("https://th.bing.com/th/id/OIP.2FrJJHlpdyhFq91ka5s65QHaDt?pid=Api&rs=1")
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
            .circleCrop()
            .into(profileImageView)

        Glide.with(this)
            .load("https://th.bing.com/th/id/OIP.dJDwTz9MBwwzzXTdpacPLQHaEo?pid=Api&rs=1")
            .error(R.drawable.ic_launcher_background)
            .placeholder(R.drawable.ic_launcher_background)
            .into(coverImageView)
    }

}